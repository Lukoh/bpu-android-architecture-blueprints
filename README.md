# BPU Android Architecture 1.1 Blueprint
-------------

![루코님 비피유1.png](https://bitbucket.org/repo/z8jnRkk/images/1759200338-%E1%84%85%E1%85%AE%E1%84%8F%E1%85%A9%E1%84%82%E1%85%B5%E1%86%B7%20%E1%84%87%E1%85%B5%E1%84%91%E1%85%B5%E1%84%8B%E1%85%B21.png)

The Android framework provides a lot of flexibility in deciding how to organize and architect all BPU Android Apps. While this freedom is very valuable, it can also lead to apps with large classes, inconsistent naming schemes, as well as mismatching or missing architectures. These types of issues can make testing, maintaining and extending your apps difficult.

The Android Architecture Blueprint project demonstrates strategies to help solve or avoid these common problems. This project implements the same app using different architectural concepts and tools.

You can use all source code in this project as a learning reference, or as a starting point for creating BPU Android apps. The focus of this project is on demonstrating how to structure your code, design your architecture, and the eventual impact of adopting these patterns on testing and maintaining BPU Android app. You can use the techniques demonstrated here in many different ways to build apps. Your own particular priorities will impact how you implement the concepts in these projects, so you should not consider these samples to be canonical examples. To ensure the focus in kept on the aims described above, the app uses a simple UI.


### Summary ###
------------
BPU Base Android Architecture consist of __Presentation layer__, __Domain layer__ and __Repository layer__. And new latest technologies, __Clean Architecture__ + __Dagger 2.0__ + __MVP design pattern__, were applied into __BPU Base Android Architecture__. These stuff make BPU Android Apps to be extended being more competitive power and help them to maintain consistency. 

#### BPU Base Android Architecture ###
![BPU Android Architecture.png](https://bitbucket.org/repo/z8jnRkk/images/3554603211-BPU%20Android%20Architecture.png)
#### Data Communication Diagram ###
![DATA COMMUNICATION DIAGRAM.png](https://bitbucket.org/repo/z8jnRkk/images/782106157-DATA%20COMMUNICATION%20DIAGRAM.png)

#### EventBus ###
EventBus is a publish/subscribe event bus optimized for Android.
![Event Bus.png](https://bitbucket.org/repo/z8jnRkk/images/4216441710-Event%20Bus.png)

This BPU Android Architecture Blueprints project stands on the principles of __Clean Architecture__ + __MVP__ + __Dagger 2.0__.
All BPU Android apps have to be built being based on the BPU Android Architecture Blueprints project.

* [Clean Architecture](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/Clean+Architecture) : Clean Architecture is essential reading for every software architect, systems analyst, system designer, and    software manager - and for any programmer who aspires to these roles or is impacted by their work. 

    * [Domain Layer](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/Clean+Architecture) : Holds all business logic. The domain layer starts with classes named use cases or interactor used by the application 
 presenters. These use cases represent all the possible actions a developer can perform from the presentation layer.

         * Interactor : All the external components use Interactor's interfaces when connecting to the business objects. Interactor provides interfaces to access to all business logic. That means Interactor is coupled with Usecase. 
All interfaces in Interactor used by the application presenters.  
   
    * [Repository Layer](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/Clean+Architecture) : All data needed for the application comes from this layer through a Communicator implementation (the interface is in the domain layer) that uses a Repository Pattern with a strategy that, through a factory, picks different data sources depending on certain conditions.

         * Data Helper : Data Helper provides developers with a simple way to manipulate internal data such as Preference data or configuration data for setting with an existing data handling modules and to manage its initial creation and any upgrades.

    * [Presentation Layer](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/Clean+Architecture) : Fragments and activities are only views, there is no logic inside them other than UI logic, and this is where all the rendering stuff takes place. 

* [MVP](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/MVP+Design+Pattern) : Model View Presenter design pattern - MVP (Model View Presenter) pattern is a derivative from the well known MVC (Model View Controller), which for a while now is gaining importance in the development of Android applications.

* [Dagger 2.0](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/Dagger+2.0) : Dagger is a fully static, compile-time dependency injection framework and is a compile-time evolution approach to dependency injection. Dagger enable __our project’s performance to improve over 15 %__ if we could apply
it to our project.

* [Auto Value](https://bpuholdings.atlassian.net/wiki/display/lukoh1004/AutoValue) : Google’s AutoValue library makes them much easier and has just received the long awaited update that adds the flexibility of extensions. It enables you to focus on implementing your core code and cut back on the amount of your code like boilerplate code. 

* [Event Bus](http://greenrobot.org/eventbus/) : EventBus is an open-source library for Android using the publisher/subscriber pattern for loose coupling. EventBus enables central communication to decoupled classes – simplifying the code, removing dependencies, and speeding up app development.

    EventBus...

    * simplifies the communication between components
        * decouples event senders and receivers
        * performs well with Activities, Fragments, and background threads
        * avoids complex and error-prone dependencies and life cycle issues
    * makes your code simpler
    * is fast
    * is tiny (~50k jar)
    * is proven in practice by apps with 100,000,000+ installs
    * has advanced features like delivery threads, subscriber priorities, etc.

### Technologies to be applied in the future
------------
* [RxJava](https://github.com/ReactiveX/RxJava/wiki) : RxJava is a Java VM implementation of ReactiveX (Reactive Extensions) : A library for composing asynchronous and event-based programs by using observable sequences.

* [RxAndroid](https://github.com/ReactiveX/RxAndroid) : Reactive Extensions for Android : This module adds the minimum classes to RxJava that make writing reactive components in Android applications easy and hassle-free.

### Requirements ###
------------
* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Android 5.0](https://developer.android.com/studio/releases/platforms.html)- __BPU Android Architecture__ supports over Android 5.0
* Latest Android SDK Tools and build tools.

### Dependencies ###
------------
All libraries below enable BPU Android App to be efficient and improve the performance. That means you have to use those libraries to make BPU Android App.

#### Leverages third-party libraries: ####

 * [Retrofit](http://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java(For asynchronous network requests)
 * [EventBus](http://greenrobot.org/eventbus/) - For communication between Activities, Fragments, Service, etc
 * [ButterKnife](http://jakewharton.github.io/butterknife/) - For field and method binding for Android views
 * [Glide](https://github.com/bumptech/glide) - For an image loading and caching library for Android focused on smooth scrolling
 * [SwipyRefreshLayout](https://github.com/OrangeGangsters/SwipyRefreshLayout) - For swiping in both direction
 * [MaterialDrawer](https://github.com/mikepenz/MaterialDrawer) - For using drawer menu
 * [SlidingDrawer]( https://github.com/Lukoh/Fyber_challenge_android/blob/master/app/src/main/java/com/goforer/fyber_challenge/ui/view/drawer/SlidingDrawer.java) - I added/implemented more good containers to be input into MaterialDrawer. Please refer to [SlidingDrawer]( https://github.com/Lukoh/Fyber_challenge_android/blob/master/app/src/main/java/com/goforer/fyber_challenge/ui/view/drawer/SlidingDrawer.java) 

### Code Convention ###
------------
#### Android Code Style ####
 * Code Style Rules : Android follows standard Java coding conventions with the additional rules as link below.

[Additional Coding Style Rules](https://source.android.com/source/code-style.html)