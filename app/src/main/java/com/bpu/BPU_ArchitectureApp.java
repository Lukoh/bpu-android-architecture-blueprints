package com.bpu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.ImageView;

import com.bpu.BPU_Architecture_Blueprint.R;
import com.bpu.BPU_Architecture_Blueprint.dagger.component.AppComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.component.DaggerAppComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.AppModule;
import com.bpu.BPU_Architecture_Blueprint.presentation.exception.ExceptionHandler;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.view.drawer.loader.AbstractSlidingDrawerImageLoader;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.view.drawer.loader.SlidingDrawerImageLoader;
import com.bumptech.glide.Glide;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;

public class BPU_ArchitectureApp extends MultiDexApplication {
    private static final String TAG = "BPU_ArchitectureApp";

    private AppComponent mAppComponent;

    @SuppressLint("StaticFieldLeak")
    public static Context mContext;
    public static Resources mResources;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        mAppComponent.getApplication(this);
        mAppComponent.getApplicationContext(this);

        mContext = getApplicationContext();
        mResources = getResources();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Thread.UncaughtExceptionHandler exceptionHandler =
                        Thread.getDefaultUncaughtExceptionHandler();
                try {
                    exceptionHandler = new ExceptionHandler(BPU_ArchitectureApp.this,
                            exceptionHandler, new ExceptionHandler.OnFindCrashLogListener() {

                                @Override
                                public void onFindCrashLog(String log) {
                                    Log.e("onFindCrashLog", log);
                                }

                                @Override
                                public void onCaughtCrash(Throwable throwable) {
                                    Log.e(TAG, "Ooooooops Crashed!!");
                                }
                            });

                    Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
                } catch( NullPointerException e ) {
                    e.printStackTrace();
                }
            }
        }).start();

        //initialize and create the image loader logic
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Glide.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                //define different placeholders for different imageView targets
                //default tags are accessible via the DrawerImageLoader.Tags
                //custom ones can be checked via string. see the CustomUrlBasePrimaryDrawerItem LINE 111
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
                    return DrawerUIUtils.getPlaceHolder(ctx);
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(
                            com.mikepenz.materialdrawer.R.color.primary).sizeDp(56);
                } else if ("customUrlItem".equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(
                            R.color.md_red_500).sizeDp(56);
                }

                //we use the default one for
                //DrawerImageLoader.Tags.PROFILE_DRAWER_ITEM.name()

                return super.placeholder(ctx, tag);
            }
        });

        SlidingDrawerImageLoader.init(new AbstractSlidingDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Glide.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                //define different placeholders for different imageView targets
                //default tags are accessible via the DrawerImageLoader.Tags
                //custom ones can be checked via string. see the CustomUrlBasePrimaryDrawerItem LINE 111
                return super.placeholder(ctx, tag);
            }
        });

    }

    public static void closeApplication() {
        System.exit(0);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(BPU_ArchitectureApp.this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @SuppressWarnings("unused")
    public AppComponent getAndroidComponent() {
        return mAppComponent;
    }

    public BPU_ArchitectureApp getApplication() {
        return this;
    }
}

