package com.bpu.base.presentation.presenter;

public interface BasePresenter {
    void start();
    void stop();
}
