package com.bpu.BPU_Architecture_Blueprint.presentation.contract;

import android.content.Context;

import com.bpu.base.presentation.presenter.BasePresenter;
import com.bpu.base.presentation.view.BaseView;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.User;

public interface SplashContract {
    interface View extends BaseView<SplashContract.Presenter> {
        void setUserProfile(User user);

        void showError(Context context, String errorMessage);
    }

    interface Presenter extends BasePresenter {
        void getUser(String userName);
    }
}
