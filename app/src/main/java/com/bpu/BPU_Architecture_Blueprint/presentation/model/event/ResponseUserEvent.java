package com.bpu.BPU_Architecture_Blueprint.presentation.model.event;

import com.bpu.base.presentation.model.event.ResponseEvent;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.SplashContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.User;

public class ResponseUserEvent extends ResponseEvent{
    protected User mResponse;

    protected View mView;

    protected String mMessage;

    @SuppressWarnings("unused")
    public User getResponseClient() { return mResponse; }

    public void setResponseClient(User responses, String message) {
        mResponse = responses;
        mMessage = message;
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        mView = view;
    }

    public String getMessage() {
        return mMessage;
    }
}
