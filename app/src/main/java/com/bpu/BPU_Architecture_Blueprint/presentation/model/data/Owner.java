package com.bpu.BPU_Architecture_Blueprint.presentation.model.data;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
@SuppressWarnings("WeakerAccess")
public abstract class Owner implements Parcelable {
    @SerializedName("id")
    public abstract long mId();
    @SerializedName("avatar_url")
    @Nullable
    public abstract String mAvartarUrl();
    @SerializedName("repos_url")
    @Nullable
    public abstract String mReposUrl();

    @SuppressWarnings("WeakerAccess")
    public static TypeAdapter<Owner> typeAdapter(Gson gson) {
        return new AutoValue_Owner.GsonTypeAdapter(gson);
    }

    public static Owner create(long mId, String mAvartarUrl, String mReposUrl) {
        return builder()
                .mId(mId)
                .mAvartarUrl(mAvartarUrl)
                .mReposUrl(mReposUrl)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_Owner.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder mId(long mId);

        public abstract Builder mAvartarUrl(String mAvartarUrl);

        public abstract Builder mReposUrl(String mReposUrl);

        public abstract Owner build();
    }
}
