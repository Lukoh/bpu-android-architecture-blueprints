package com.bpu.BPU_Architecture_Blueprint.presentation.view.view.drawer.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpu.BPU_Architecture_Blueprint.R;

@SuppressWarnings("unused")
public class MenuViewHolder extends RecyclerView.ViewHolder {
    protected View mView;
    protected ImageView mIcon;
    protected TextView mMenu;
    protected TextView mMenu_sub1;
    protected TextView mMenu_sub2;
    protected TextView mDescription;

    public MenuViewHolder(View view) {
        super(view);

        mView = view;
        mIcon = (ImageView) view.findViewById(R.id.material_drawer_icon);
        mMenu = (TextView) view.findViewById(R.id.material_drawer_menu);
        mMenu_sub1 = (TextView) view.findViewById(R.id.material_drawer_menu_sub1);
        mMenu_sub2 = (TextView) view.findViewById(R.id.material_drawer_menu_sub2);
        mDescription = (TextView) view.findViewById(R.id.material_drawer_menu_description);
    }

    public View getView() {
        return mView;
    }

    public ImageView getIcon() {
        return mIcon;
    }

    public TextView getMenu() {
        return mMenu;
    }

    public TextView getMenuSub1() {
        return mMenu_sub1;
    }

    public TextView getMenuSub2() {
        return mMenu_sub2;
    }

    public TextView getDescription() {
        return mDescription;
    }
}
