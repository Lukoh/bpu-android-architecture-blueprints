package com.bpu.BPU_Architecture_Blueprint.presentation.model.data;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bpu.base.presentation.model.BaseModel;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class User extends BaseModel implements Parcelable {
    @SerializedName("id")
    public abstract long mId();
    @SerializedName("avatar_url")
    @Nullable
    public abstract String mAvatarUrl();
    @SerializedName("gravatar_id")
    @Nullable
    public abstract String mAvatarId();
    @SerializedName("url")
    @Nullable
    public abstract String mUrl();
    @SerializedName("name")
    @Nullable
    public abstract String mName();
    @SerializedName("email")
    @Nullable
    public abstract String mEmail();
    @SerializedName("company")
    @Nullable
    public abstract String mCompany();
    @SerializedName("blog")
    @Nullable
    public abstract String mBlog();
    @SerializedName("location")
    @Nullable
    public abstract String mLocation();
    @SerializedName("public_repos")
    public abstract int mReposCount();
    @SerializedName("followers")
    public abstract int mFollowers();
    @SerializedName("following")
    public abstract int mFollowing();

    @SuppressWarnings("WeakerAccess")
    public static TypeAdapter<User> typeAdapter(Gson gson) {
        return new AutoValue_User.GsonTypeAdapter(gson);
    }

    public static User create(long mId, String mAvatarUrl, String mAvatarId, String mUrl,
                              String mName, String mEmail, String mCompany, String mBlog,
                              String mLocation, int mReposCount, int mFollowers, int mFollowing) {
        return builder()
                .mId(mId)
                .mAvatarUrl(mAvatarUrl)
                .mAvatarId(mAvatarId)
                .mUrl(mUrl)
                .mName(mName)
                .mEmail(mEmail)
                .mCompany(mCompany)
                .mBlog(mBlog)
                .mLocation(mLocation)
                .mReposCount(mReposCount)
                .mFollowers(mFollowers)
                .mFollowing(mFollowing)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_User.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder mId(long mId);

        public abstract Builder mAvatarUrl(String mAvatarUrl);

        public abstract Builder mAvatarId(String mAvatarId);

        public abstract Builder mUrl(String mUrl);

        public abstract Builder mName(String mName);

        public abstract Builder mEmail(String mEmail);

        public abstract Builder mCompany(String mCompany);

        public abstract Builder mBlog(String mBlog);

        public abstract Builder mLocation(String mLocation);

        public abstract Builder mReposCount(int mReposCount);

        public abstract Builder mFollowers(int mFollowers);

        public abstract Builder mFollowing(int mFollowing);

        public abstract User build();
    }
}