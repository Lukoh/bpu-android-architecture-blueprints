package com.bpu.BPU_Architecture_Blueprint.presentation.model.data;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bpu.base.presentation.model.BaseModel;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class Repository extends BaseModel implements Parcelable {
    @SerializedName("id")
    public abstract long mId();
    @SerializedName("name")
    @Nullable
    public abstract String mName();
    @SerializedName("owner")
    @Nullable
    public abstract Owner mOwner();
    @SerializedName("description")
    @Nullable
    public abstract String mDescription();
    @SerializedName("url")
    @Nullable
    public abstract String mUrl();
    @SerializedName("homepage")
    @Nullable
    public abstract String mHomepage();
    @SerializedName("stargazers_count")
    public abstract int mStarCount();

    @SuppressWarnings("WeakerAccess")
    public static TypeAdapter<Repository> typeAdapter(Gson gson) {
        return new AutoValue_Repository.GsonTypeAdapter(gson);
    }

    public static Repository create(long mId, String mName, Owner mOwner, String mDescription,
                                    String mUrl, String mHomepage, int mStarCount) {
        return builder()
                .mId(mId)
                .mName(mName)
                .mOwner(mOwner)
                .mDescription(mDescription)
                .mUrl(mUrl)
                .mHomepage(mHomepage)
                .mStarCount(mStarCount)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_Repository.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder mId(long mId);

        public abstract Builder mName(String mName);

        public abstract Builder mOwner(Owner mOwner);

        public abstract Builder mDescription(String mDescription);

        public abstract Builder mUrl(String mUrl);

        public abstract Builder mHomepage(String mHomepage);

        public abstract Builder mStarCount(int mStarCount);

        public abstract Repository build();
    }
}
