package com.bpu.BPU_Architecture_Blueprint.presentation.contract;

import com.bpu.base.presentation.presenter.BasePresenter;
import com.bpu.base.presentation.view.BaseView;

import java.util.List;

public interface RepositoryAdapterContract {
    interface View extends BaseView<RepositoryContract.Presenter> {
        void addItems(List<?> list, boolean isUpdated);

        void showError(String errorMessage);
    }

    interface Presenter extends BasePresenter {
        void callHomepage(String homepage);
    }
}
