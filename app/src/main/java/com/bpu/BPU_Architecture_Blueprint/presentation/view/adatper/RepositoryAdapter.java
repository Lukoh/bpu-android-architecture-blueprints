package com.bpu.BPU_Architecture_Blueprint.presentation.view.adatper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bpu.base.presentation.utils.CommonUtils;
import com.bpu.base.presentation.view.activity.BaseActivity;
import com.bpu.base.presentation.view.adatper.BaseListAdapter;
import com.bpu.base.presentation.view.fragment.RecyclerFragment;
import com.bpu.base.presentation.view.helper.ItemTouchHelperListener;
import com.bpu.base.presentation.view.holder.BaseViewHolder;
import com.bpu.base.presentation.view.holder.DefaultViewHolder;
import com.bpu.base.presentation.view.view.SquircleImageView;
import com.bpu.BPU_Architecture_Blueprint.R;
import com.bpu.BPU_ArchitectureApp;
import com.bpu.BPU_Architecture_Blueprint.presentation.caller.Caller;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryContract;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.Repository;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class  RepositoryAdapter extends BaseListAdapter<Repository>
        implements RepositoryAdapterContract.Presenter, RepositoryAdapterContract.View,
                   ItemTouchHelperListener {

    @SuppressWarnings("unused")
    private RepositoryContract.Presenter mPresenter;

    private Context mContext;

    private RecyclerFragment mFragment;

    private List mItems;

    @Inject
    public RepositoryAdapter(Context context, RecyclerFragment fragment) {
        super(R.layout.list_repository_item);

        mContext = context;
        mFragment = fragment;
        mItems = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        int count  = super.getItemCount();

        if (isReachedToLastPage()) {
            return count + 1;
        }

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int itemCount = getItemCount() - 1;

        if (isReachedToLastPage() && position == itemCount) {
            return VIEW_TYPE_FOOTER;
        }

        return VIEW_TYPE_ITEM;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        View view;

        switch (type) {
            case VIEW_TYPE_FOOTER:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_last_item,
                        viewGroup, false);
                return new DefaultViewHolder(view);
            default:
                return super.onCreateViewHolder(viewGroup, type);
        }
    }

    @Override
    protected BaseViewHolder createViewHolder(ViewGroup viewGroup, View view, int type) {
        return new RepositoryViewHolder(view, mPresenter, ((BaseActivity)view.getContext()).resumed());
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder viewHolder, int position) {
        switch (getItemViewType(position)){
            case VIEW_TYPE_FOOTER:
            case VIEW_TYPE_LOADING:
                return;
            default:
                super.onBindViewHolder(viewHolder, position);
        }
    }

    @Override
    public void onItemDismiss(int position) {
        getItems().remove(position);
        notifyItemRemoved(position);
        notifyItemChanged(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(getItems(), fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        notifyItemChanged(toPosition);
        notifyItemChanged(fromPosition);

        return true;
    }

    @Override
    public void onItemDrag(RecyclerFragment fragment, int actionState) {
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            fragment.getRefreshLayout().setRefreshing(false);
            fragment.getRefreshLayout().setEnabled(false);
        } else if (actionState == ItemTouchHelper.ACTION_STATE_IDLE){
            fragment.getRefreshLayout().setEnabled(true);
        }
    }

    @Override
    public void start() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void stop() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void setPresenter(RepositoryContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addItems(final List<?> items, boolean isUpdated) {
        if (items != null && !items.isEmpty()) {
            if (isUpdated) {
                mItems.addAll(0, items);
                notifyItemRangeChanged(0, items.size() + 1);
            } else {
                int startIndex = items.size() - 1;
                mItems.addAll(items);
                if (mFragment.getCurrentPage() != 1) {
                    notifyItemRangeChanged(startIndex, items.size() + 1);
                } else {
                    notifyDataSetChanged();
                }
            }
        } else {
            notifyDataSetChanged();
        }

        super.addItems(mItems);

        mFragment.doneRefreshing();
    }

    @Override
    public void showError(String errorMessage) {
        CommonUtils.showToastMessage(mContext, errorMessage, Toast.LENGTH_SHORT);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BPU_ArchitectureApp.closeApplication();
            }
        }, Toast.LENGTH_SHORT);
    }

    @Override
    public void callHomepage(String homepage) {
        if ((homepage == null) || "".equals(homepage)) {
            CommonUtils.showToastMessage(mContext,
                    mContext.getString(R.string.no_homepage), Toast.LENGTH_SHORT);

            return;
        }

        Caller.INSTANCE.callChromeCustomTabs(mContext, homepage);

    }

    public void setEnableLoadingImage(boolean usedLoadingImage) {
        setUsedLoadingImage(usedLoadingImage);
    }

    private final static class RepositoryViewHolder extends BaseViewHolder<Repository> {
        private final View mView;

        private final RepositoryContract.Presenter mPresenter;

        @SuppressWarnings("unused")
        private boolean mIsResumed;

        RepositoryViewHolder(final View itemView, @NonNull final RepositoryContract.Presenter presenter,
                             boolean isResumed) {
            super(itemView);

            mView = itemView;
            mPresenter = presenter;
            mIsResumed = isResumed;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void bindItemHolder(final BaseViewHolder holder, @NonNull final Repository repository,
                                   final int position) {
            holder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsResumed) {
                        mPresenter.getRepositoryAdapterPresenter().callHomepage(
                                repository.mHomepage());
                    }
                }
            });

            ((TextView)holder.getView().findViewById(R.id.tv_name))
                    .setText(repository.mName());
            ((TextView)holder.getView().findViewById(R.id.tv_description))
                    .setText(repository.mDescription());
            ((TextView)holder.getView().findViewById(R.id.tv_count))
                    .setText(holder.getContext().getString(R.string.star_count)
                            +  " " + String.valueOf(repository.mStarCount()));
            ((SquircleImageView)holder.getView().findViewById(R.id.iv_avatar))
                    .setImage(repository.mOwner().mAvartarUrl());
        }

        @Override
        public void onItemSelected() {
            mView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            mView.setBackgroundColor(0);
        }
    }

}
