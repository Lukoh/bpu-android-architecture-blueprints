package com.bpu.BPU_Architecture_Blueprint.dagger.module.activity;

import com.bpu.base.presentation.view.BaseView;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.SplashContract;
import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * A module for Android-specific dependencies which require a {@link BaseView}
 */
@Module
public class SplashActivityModule {
    private SplashContract.View mView;

    public SplashActivityModule(SplashContract.View view) {
        mView = view;
    }

    @Provides
    @PerActivity
    SplashContract.View providesSplashContractView() {
        return mView;
    }
}
