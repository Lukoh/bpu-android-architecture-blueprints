package com.bpu.BPU_Architecture_Blueprint.dagger.component.network;

import com.bpu.BPU_Architecture_Blueprint.dagger.module.network.NetModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(
        modules = NetModule.class
)
public interface NetComponent {
    // downstream components need these exposed
    // method name does not really matter
    Retrofit retrofit();
}

