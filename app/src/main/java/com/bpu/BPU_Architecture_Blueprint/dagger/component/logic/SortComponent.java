package com.bpu.BPU_Architecture_Blueprint.dagger.component.logic;

import com.bpu.BPU_Architecture_Blueprint.domain.sort.impl.SortImpl;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.logic.SortModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = SortModule.class
)
public interface SortComponent {
    // downstream components need these exposed with the return type
    // method name does not really matter
    SortImpl sort();
}
