package com.bpu.BPU_Architecture_Blueprint.dagger.module.logic;

import com.bpu.BPU_Architecture_Blueprint.domain.sort.impl.SortImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SortModule {
    @Provides
    @Singleton
    SortImpl providesSort() {
        return new SortImpl<>();
    }
}
