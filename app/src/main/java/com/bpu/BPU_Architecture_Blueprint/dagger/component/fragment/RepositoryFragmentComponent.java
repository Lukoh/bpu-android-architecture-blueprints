package com.bpu.BPU_Architecture_Blueprint.dagger.component.fragment;

import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.PerFragment;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.AppModule;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.fragment.RepositoryFragmentModule;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.fragment.RepositoryFragment;

import dagger.Component;

@PerFragment
@Component(
        dependencies = AppModule.class,
        modules = RepositoryFragmentModule.class
)
public interface RepositoryFragmentComponent {
    void inject(RepositoryFragment repositoryFragment);
}