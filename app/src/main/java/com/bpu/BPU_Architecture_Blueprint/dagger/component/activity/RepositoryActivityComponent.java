package com.bpu.BPU_Architecture_Blueprint.dagger.component.activity;

import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.PerActivity;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.AppModule;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.activity.RepositoryActivityModule;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.activity.RepositoryActivity;

import dagger.Component;

@PerActivity
@Component(
        dependencies = AppModule.class,
        modules = RepositoryActivityModule.class
)
public interface RepositoryActivityComponent {
    void inject(RepositoryActivity repositoryActivity);
}
