package com.bpu.BPU_Architecture_Blueprint.dagger.module.activity;

import android.content.Context;

import com.bpu.base.presentation.view.BaseView;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryContract;
import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * A module for Android-specific dependencies which require a {@link BaseView}
 */
@Module
public class RepositoryActivityModule {
    @SuppressWarnings("unused")
    private final Context mContext;

    private final RepositoryContract.View mView;

    public RepositoryActivityModule(Context context, RepositoryContract.View view) {
        mContext = context;
        mView = view;
    }

    @Provides
    @PerActivity
    RepositoryContract.View providesRepositoryContractView() {
        return mView;
    }

}
