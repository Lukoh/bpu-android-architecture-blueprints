package com.bpu.BPU_Architecture_Blueprint.dagger.component.activity;

import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.PerActivity;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.AppModule;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.activity.SplashActivityModule;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.activity.SplashActivity;

import dagger.Component;

@PerActivity
@Component(
        dependencies = AppModule.class,
        modules = SplashActivityModule.class
)
public interface SplashActivityComponent {
     void inject(SplashActivity splashActivity);
}
