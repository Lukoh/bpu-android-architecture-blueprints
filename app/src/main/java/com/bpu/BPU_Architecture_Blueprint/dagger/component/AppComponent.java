package com.bpu.BPU_Architecture_Blueprint.dagger.component;

import android.content.Context;

import com.bpu.BPU_ArchitectureApp;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = AppModule.class
)
public interface AppComponent {
    @SuppressWarnings("unused")
    BPU_ArchitectureApp getApplication(BPU_ArchitectureApp application);

    @SuppressWarnings("unused")
    Context getApplicationContext(Context context);
}
