package com.bpu.BPU_Architecture_Blueprint.dagger.component.network;

import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.NetScope;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.network.GithubModule;
import com.bpu.BPU_Architecture_Blueprint.repository.request.BaseReqImpl;

import dagger.Component;

@NetScope
@Component(
        dependencies = NetComponent.class,
        modules = GithubModule.class
)
public interface GithubComponent {
    void inject(BaseReqImpl baseReqImpl);
}
