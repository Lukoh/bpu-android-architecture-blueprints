package com.bpu.BPU_Architecture_Blueprint.dagger.module;

import android.app.Application;
import android.content.Context;

import com.bpu.BPU_ArchitectureApp;
import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.ForApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * A module for Android-specific dependencies which require a {@link Context} or
 * {@link android.app.Application} to create.
 */
@Module
public class AppModule {
    private BPU_ArchitectureApp mApplication;

    public AppModule(BPU_ArchitectureApp application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    @ForApplication
    public Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return mApplication;
    }
}
