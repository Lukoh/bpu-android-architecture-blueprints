package com.bpu.BPU_Architecture_Blueprint.dagger.module.network;


import com.bpu.BPU_Architecture_Blueprint.dagger.annotation.scope.NetScope;
import com.bpu.BPU_Architecture_Blueprint.repository.communicator.RequestMethod;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class GithubModule {
    private RequestMethod mRequestMethod;

    @Provides
    @NetScope
    RequestMethod providesRequestMethod(Retrofit retrofit) {
        if (mRequestMethod == null) {
            mRequestMethod = retrofit.create(RequestMethod.class);
        }

        return mRequestMethod;
    }
}
