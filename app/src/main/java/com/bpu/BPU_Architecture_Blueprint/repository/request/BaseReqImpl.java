package com.bpu.BPU_Architecture_Blueprint.repository.request;

import com.bpu.BPU_Architecture_Blueprint.dagger.component.network.DaggerGithubComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.component.network.DaggerNetComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.component.network.NetComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.network.GithubModule;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.network.NetModule;
import com.bpu.BPU_Architecture_Blueprint.repository.communicator.RequestMethod;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class BaseReqImpl {
    private static final String BASE_URL = "https://api.github.com";

    @Inject
    Retrofit mRetrofit;

    @Inject
    RequestMethod mRequestMethod;

    BaseReqImpl() {
        NetComponent netComponent = DaggerNetComponent.builder()
                .netModule(new NetModule(BASE_URL))
                .build();

        DaggerGithubComponent.builder().netComponent(netComponent)
                .githubModule(new GithubModule())
                .build()
                .inject(this);
    }
}
