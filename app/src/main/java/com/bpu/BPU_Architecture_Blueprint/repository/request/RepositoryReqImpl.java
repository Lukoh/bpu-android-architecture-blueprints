package com.bpu.BPU_Architecture_Blueprint.repository.request;

import android.content.Context;

import com.bpu.BPU_Architecture_Blueprint.domain.repository.RepositoryReq;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.Repository;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.event.ResponseRepositoryEvent;
import com.bpu.BPU_Architecture_Blueprint.repository.communicator.callback.RequestRepositoryCallback;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RepositoryReqImpl extends BaseReqImpl implements RepositoryReq {

    public RepositoryReqImpl() {
        super();
    }

    @Override
    public void getRepositoryList(final Context context, final View view, final String userName,
                                  final boolean enabledSort, final ResponseRepositoryEvent event) {
        Call<List<Repository>> call = mRequestMethod.getRepository(userName);
        call.enqueue(new RequestRepositoryCallback(context, event, view, enabledSort) {
            @Override
            public void onResponse(Call<List<Repository>> call,
                                   Response<List<Repository>> response) {
                super.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }
}
