package com.bpu.BPU_Architecture_Blueprint.repository.request;

import com.bpu.BPU_Architecture_Blueprint.domain.repository.UserReq;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.SplashContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.User;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.event.ResponseUserEvent;
import com.bpu.BPU_Architecture_Blueprint.repository.communicator.callback.RequestUserCallback;

import retrofit2.Call;
import retrofit2.Response;

public class UserReqImpl extends BaseReqImpl implements UserReq {

    public UserReqImpl() {
        super();
    }

    @Override
    public void getProfile(final View view, final String userName, final ResponseUserEvent event)  {

        Call<User> call = mRequestMethod.getUser(userName);
        call.enqueue(new RequestUserCallback(event, view) {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                super.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

}
