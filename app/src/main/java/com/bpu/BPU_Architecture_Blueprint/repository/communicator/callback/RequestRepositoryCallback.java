package com.bpu.BPU_Architecture_Blueprint.repository.communicator.callback;

import android.content.Context;

import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.Repository;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.event.ResponseRepositoryEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Communicates responses from Server or offline requests.
 * One and only one method will be invoked in response to a given request.
 */
public class RequestRepositoryCallback implements Callback<List<Repository>> {
    private ResponseRepositoryEvent mEvent;

    private View mView;

    private Context mContext;

    private boolean mEnabledSort;

    protected RequestRepositoryCallback(final Context context, final ResponseRepositoryEvent event,
                                        final View view, final boolean enabledSort) {
        mContext = context;
        mEvent = event;
        mView = view;
        mEnabledSort = enabledSort;
    }

    @Override
    public void onResponse(Call<List<Repository>> call,
                           retrofit2.Response<List<Repository>> response) {
        if (mEvent != null) {
            mEvent.setResponseClient(response.body(), "SUCCESSFUL");
            mEvent.parseInResponse();
            mEvent.setContext(mContext);
            mEvent.setView(mView);
            mEvent.setEnableSort(mEnabledSort);
            EventBus.getDefault().post(mEvent);
        }
    }

    @Override
    public void onFailure(Call<List<Repository>> call, Throwable t) {
        if (mEvent != null) {
            mEvent.setResponseClient(null, t.getMessage());
            mEvent.setView(mView);
            EventBus.getDefault().post(mEvent);
        }
    }
}