package com.bpu.BPU_Architecture_Blueprint.domain.repository;

import com.bpu.BPU_Architecture_Blueprint.presentation.contract.SplashContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.event.ResponseUserEvent;

public interface UserReq {
    void getProfile(View view, String userName, ResponseUserEvent event);
}
