package com.bpu.BPU_Architecture_Blueprint.domain.sort;

import java.util.Comparator;
import java.util.List;

public interface Sort<T> {
    void sort(List<T> items, Comparator<T> comparator);
}