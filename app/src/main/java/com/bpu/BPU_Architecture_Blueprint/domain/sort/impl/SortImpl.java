package com.bpu.BPU_Architecture_Blueprint.domain.sort.impl;

import com.bpu.BPU_Architecture_Blueprint.domain.sort.Sort;
import com.bpu.BPU_Architecture_Blueprint.presentation.presenter.RepositoryPresenter;
import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract.View;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.bpu.BPU_Architecture_Blueprint.annotation.ForUnitTest;

import javax.inject.Inject;

public class SortImpl<T> implements Sort<T> {
    private RepositoryPresenter mRepositoryPresenter;

    private View mView;

    @Inject
    public SortImpl() {

    }

    @Override
    public void sort(List<T> items, Comparator<T> comparator) {
        if (items != null && !items.isEmpty()) {
            Collections.sort(items, comparator);
        }

        mRepositoryPresenter.onSorted(mView, items);
    }

    public void setSort(RepositoryPresenter presenter, View view) {
        mRepositoryPresenter = presenter;
        mView = view;
    }

    @ForUnitTest
    public List<T> testSort(List<T> items, Comparator<T> comparator) {
        if (items != null && !items.isEmpty()) {
            Collections.sort(items, comparator);
        }

        return items;
    }
}
