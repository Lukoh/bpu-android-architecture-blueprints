package com.bpu.BPU_Architecture_Blueprint.domain.sort.comparator;

import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.Repository;

import java.util.Comparator;

public class RepositoryComparator implements Comparator<Repository> {
    public RepositoryComparator() {
    }

    @Override
    public int compare(Repository repos1, Repository repos2) {
        return repos2.mStarCount() - repos1.mStarCount();
    }
}
