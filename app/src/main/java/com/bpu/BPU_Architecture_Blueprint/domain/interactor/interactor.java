package com.bpu.BPU_Architecture_Blueprint.domain.interactor;

import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract;

import java.util.List;

public interface interactor {
    void onSorted(RepositoryAdapterContract.View view, List<?> items);
}
