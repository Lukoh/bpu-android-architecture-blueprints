package com.bpu.BPU_Architecture_Blueprint.domain.repository;

import android.content.Context;

import com.bpu.BPU_Architecture_Blueprint.presentation.contract.RepositoryAdapterContract.View;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.event.ResponseRepositoryEvent;

public interface RepositoryReq {
    void getRepositoryList(Context context, View view, String userName, boolean enabledSort,
                           ResponseRepositoryEvent event);
}
