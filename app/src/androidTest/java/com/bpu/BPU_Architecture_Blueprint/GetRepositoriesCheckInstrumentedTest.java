package com.bpu.BPU_Architecture_Blueprint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.bpu.BPU_Architecture_Blueprint.presentation.caller.Caller;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.User;
import com.bpu.BPU_Architecture_Blueprint.presentation.presenter.RepositoryPresenter;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.activity.RepositoryActivity;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.adatper.RepositoryAdapter;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.fragment.RepositoryFragment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GetRepositoriesCheckInstrumentedTest {
    @SuppressWarnings("WeakerAccess")
    public static final String USER_NAME = "jakewharton"; //GrahamCampbell; jakewharton

    private RepositoryFragment mFragment;

    private User mUser;

    private Intent mIntent;

    private RepositoryAdapter mAdapter;

    @Rule
    public final ActivityTestRule<RepositoryActivity> mActivityRule =
            new ActivityTestRule<>(RepositoryActivity.class, true, false);

    /**
     * Transact an existing fragment that was added to a container.
     *
     * @param cls the component class that is to be used for BaseActivity
     * @param containerViewId Identifier of the container whose fragment(s) are to be replaced.
     * @param args Bundle of arguments to supply to the fragment
     */
    @SuppressWarnings("WeakerAccess")
    protected Fragment transactFragment(Class<?> cls, @IdRes int containerViewId, Bundle args) {
        return transactFragment(cls.getName(), containerViewId, args);
    }

    /**
     * Transact an existing fragment that was added to a container.
     *
     * @param tag Optional tag name for the fragment
     * @param containerViewId Identifier of the container whose fragment(s) are to be replaced.
     * @param args Bundle of arguments to supply to the fragment
     */
    @SuppressWarnings("WeakerAccess")
    protected Fragment transactFragment(String tag, @IdRes int containerViewId, Bundle args) {
        FragmentManager fragmentManager = mActivityRule.getActivity().getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment == null) {
            fragment = Fragment.instantiate(mActivityRule.getActivity(), tag, args);
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(containerViewId, fragment, tag);
        ft.commit();

        return fragment;
    }

    @Before
    public void createUser() {
        mUser = User.builder()
                .mId(1111)
                .mAvatarUrl("https://avatars2.githubusercontent.com/u/18302717?v=3&u=fbc486cda4c5e5e71be5b963243ff0cafc830378&s=400")
                .mAvatarId("1111")
                .mUrl("https://github.com/settings/profile")
                .mName("Lukoh")
                .mEmail("lukoh.nam@bpuholdings.com")
                .mCompany("BPU Holdings")
                .mBlog("https://github.com/Lukoh")
                .mLocation("Seoul, Korea")
                .mReposCount(10)
                .mFollowers(1000)
                .mFollowing(500)
                .build();

    }

    @Before
    public void createIntent() {
        mIntent =  new Intent();
        mIntent.putExtra(Caller.EXTRA_PROFILE, mUser);
        mIntent.putExtra(Caller.EXTRA_TEST, true);
    }

    @Before
    public void setUp() throws Exception {
        mActivityRule.launchActivity(mIntent);
        mFragment = (RepositoryFragment) transactFragment(RepositoryFragment.class, R.id.content_holder, null);
        mAdapter = new RepositoryAdapter(mActivityRule.getActivity(), mFragment);
        mAdapter.setEnableLoadingImage(true);
    }

    @Test
    public void getRepositories() {
        new RepositoryPresenter(mFragment).start();

        mFragment.getPresenter().setRepositoryAdapterView(mAdapter);
        mFragment.getPresenter().getRepositoryList(mActivityRule.getActivity(), USER_NAME, true);
    }
}
