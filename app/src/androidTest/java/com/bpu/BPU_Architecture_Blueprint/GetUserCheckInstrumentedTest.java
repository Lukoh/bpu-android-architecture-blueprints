package com.bpu.BPU_Architecture_Blueprint;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.bpu.BPU_Architecture_Blueprint.presentation.contract.SplashContract;
import com.bpu.BPU_Architecture_Blueprint.presentation.presenter.SplashPresenter;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.activity.SplashActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GetUserCheckInstrumentedTest {
    @SuppressWarnings("WeakerAccess")
    public static final String USER_NAME = "jakewharton"; //GrahamCampbell; jakewharton

    private SplashContract.Presenter mPresenter;

    /**
     * A JUnit {@link Rule @Rule} to launch your activity under test. This is a replacement
     * for {@link ActivityInstrumentationTestCase2}.
     * <p>
     * Rules are interceptors which are executed for each test method and will run before
     * any of your setup code in the {@link Before @Before} method.
     * <p>
     * {@link ActivityTestRule} will create and launch of the activity for you and also expose
     * the activity under test. To get a reference to the activity you can use
     * the {@link ActivityTestRule#getActivity()} method.
     */
    @Rule
    public ActivityTestRule<SplashActivity> mActivityRule = new ActivityTestRule<>(
            SplashActivity.class);

    @Test
    public void getUser() throws Exception {
        mPresenter = new SplashPresenter(mActivityRule.getActivity());
        mPresenter.getUser(USER_NAME);

    }
}
