package com.bpu.BPU_Architecture_Blueprint;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.bpu.BPU_Architecture_Blueprint.presentation.caller.Caller;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.User;
import com.bpu.BPU_Architecture_Blueprint.presentation.view.activity.RepositoryActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LaunchActivityInstrumentedTest {
    private User mUser;

    private Intent mIntent;

    /**
     * A JUnit {@link Rule @Rule} to launch your activity under test. This is a replacement
     * for {@link ActivityInstrumentationTestCase2}.
     * <p>
     * Rules are interceptors which are executed for each test method and will run before
     * any of your setup code in the {@link Before @Before} method.
     * <p>
     * {@link ActivityTestRule} will create and launch of the activity for you and also expose
     * the activity under test. To get a reference to the activity you can use
     * the {@link ActivityTestRule#getActivity()} method.
     */
    @Rule
    public ActivityTestRule<RepositoryActivity> mActivityRule
            = new ActivityTestRule<>(RepositoryActivity.class, true, false);

    @Before
    public void createUser() {
        mUser = User.builder()
                .mId(1111)
                .mAvatarUrl("https://avatars2.githubusercontent.com/u/18302717?v=3&u=fbc486cda4c5e5e71be5b963243ff0cafc830378&s=400")
                .mAvatarId("1111")
                .mUrl("https://github.com/settings/profile")
                .mName("Lukoh")
                .mEmail("lukoh.nam@bpuholdings.com")
                .mCompany("BPU Holdings")
                .mBlog("https://github.com/Lukoh")
                .mLocation("Seoul, Korea")
                .mReposCount(10)
                .mFollowers(1000)
                .mFollowing(500)
                .build();
    }

    @Before
    public void createIntent() {
        mIntent =  new Intent();
        mIntent.putExtra(Caller.EXTRA_PROFILE, mUser);
        mIntent.putExtra(Caller.EXTRA_TEST, true);
    }

    @Test
    public void launchActivity() {
        mActivityRule.launchActivity(mIntent);
    }
}
