package com.bpu.BPU_Architecture_Blueprint;

import android.support.test.runner.AndroidJUnit4;

import com.bpu.BPU_Architecture_Blueprint.dagger.component.logic.DaggerSortComponent;
import com.bpu.BPU_Architecture_Blueprint.dagger.module.logic.SortModule;
import com.bpu.BPU_Architecture_Blueprint.domain.sort.comparator.RepositoryComparator;
import com.bpu.BPU_Architecture_Blueprint.domain.sort.impl.SortImpl;
import com.bpu.BPU_Architecture_Blueprint.presentation.model.data.Repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class SortInstrumentedTest {
    @SuppressWarnings("WeakerAccess")
    @Inject
    SortImpl mSort;

    private List<Repository> mItems;

    @Before
    public void makeDummyData() {
        mItems = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            final Repository repository = Repository.builder()
                    .mId(i)
                    .mName("lukoh")
                    .mOwner(null)
                    .mDescription("Lukoh Test " + Integer.toString(i))
                    .mHomepage("www.bpuholdings.com")
                    .mUrl("www.bpuholdings.com")
                    .mStarCount(i)
                    .build();

            mItems.add(repository);
        }
    }

    @Before
    public void newSortInstance() {
        mSort = DaggerSortComponent.builder().sortModule(new SortModule()).build().sort();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void sort() throws Exception {
        List<Repository> items = mSort.testSort(mItems, new RepositoryComparator());

        assertEquals("Lukoh Test 9", items.get(0).mDescription());
    }
}
