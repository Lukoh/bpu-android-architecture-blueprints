package com.bpu.BPU_Architecture_Blueprint;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.bpu.base.presentation.utils.ConnectionUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NetworkCheckInstrumentedTest {
    @Test
    public void isNetworkAvailable() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals(true, ConnectionUtils.INSTANCE.isNetworkAvailable(appContext));
    }
}
